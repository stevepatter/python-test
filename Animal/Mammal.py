class Mammals:

    species = None
    """ :type str """
    name = None
    """ :type str """
    age = None
    """ :type int """

    def __init__(self, species, name, age):
        """
        Create mammal

        :param species:
        :type species: str
        :param name:
        :type name: str
        :param age:
        :type age: int
        """
        self.species = species
        self.name = name
        self.age = age
