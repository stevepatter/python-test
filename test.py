import unittest
from Dog import Dog
from Animal.Mammal import Mammals


class TestDogMethods(unittest.TestCase):

    defaultName = 'lassie'
    """ Default testing name for new instance """
    defaultAge = 4
    """ Default testing age for new instance """

    def setUp(self):
        """
        Create an initial dog instance using default class properties
        """
        self.dog = Dog(self.defaultName, self.defaultAge)

    def tearDown(self):
        """
        Perform cleanup after test run
        """
        self.dog = None

    def testInstance(self):
        """
        Verify that new instance is of the correct class
        """
        self.assertIsInstance(self.dog, Dog)

    def testName(self):
        """
        Verify that object property "name" has correct type & value
        """
        self.assertIsInstance(self.dog.name, str)
        self.assertEqual(self.dog.name, self.defaultName)

    def testAgeType(self):
        """
        Verify that object property "age" has correct type & value
        """
        self.assertIsInstance(self.dog.age, int)
        self.assertEqual(self.dog.age, self.defaultAge)

    def testDescription(self):
        """
        Verify that object method "description" returns appropriate type & value
        """
        self.assertIsInstance(self.dog.description(), str)
        self.assertIn("years old", self.dog.description())

    def testSpeak(self):
        """
        Verify that object method "speak" returns appropriate type & value
        """
        self.assertIsInstance(self.dog.speak('yap'), str)
        self.assertIn("says", self.dog.speak('yap'))


class TestMammalMethods(unittest.TestCase):

    defaultSpecies = 'cat'
    defaultName = 'jerry'
    defaultAge = 2

    def setUp(self):
        self.cat = Mammals(self.defaultSpecies, self.defaultName, self.defaultAge)

    def testInstance(self):
        self.assertIsInstance(self.cat, Mammals)

    def testSpecies(self):
        self.assertIsInstance(self.cat.species, str)
        self.assertEqual(self.cat.species, self.defaultSpecies)

    def testName(self):
        self.assertIsInstance(self.cat.name, str)
        self.assertEqual(self.cat.name, self.defaultName)

    def testAge(self):
        self.assertIsInstance(self.cat.age, int)
        self.assertEqual(self.cat.age, self.defaultAge)


if __name__ == '__main__':
    unittest.main()
