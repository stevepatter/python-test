class Dog:

    name = None
    """ :type str """
    age = None
    """ :type int """

    def __init__(self, name: str, age: int):
        """
        Create new Dog instance

        :param name: dog's name
        :type name: str
        :param age: dog's age
        :type age: int
        """
        self.name = name
        self.age = age

    def description(self) -> str:
        """
        Describe the dog

        :return: description
        :rtype: str
        """
        return "{} is {} years old".format(self.name, self.age)

    def speak(self, sound):
        """
        Ask Dog to speak a word or phrase

        :param sound:
        :type sound: str
        :return: message spoken by Dog
        :rtype: str
        """
        return "{} says {}".format(self.name, sound)
